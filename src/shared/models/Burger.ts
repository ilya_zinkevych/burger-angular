export interface Burger {
  name: string,
  price: number,
  description: string,
  energy: number,
  imgUrl: string
}