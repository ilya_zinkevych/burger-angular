import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainInfoComponent } from './main-info/main-info.component';


const menuRoutes: Routes = [
  { path: 'menu', component: MainInfoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(menuRoutes)],
  exports: [RouterModule]
})
export class MenuRoutingModule { }
