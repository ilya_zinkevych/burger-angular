import { Component, OnInit, Input } from '@angular/core';
import { Burger } from 'src/shared/models/Burger';

@Component({
  selector: 'app-burger-card',
  templateUrl: './burger-card.component.html',
  styleUrls: ['./burger-card.component.scss']
})
export class BurgerCardComponent implements OnInit {
  @Input() item: Burger;
  imgUrl
  constructor() { }

  ngOnInit() { }

}
