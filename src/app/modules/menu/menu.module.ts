import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainInfoComponent } from './main-info/main-info.component';
import { MenuRoutingModule } from './menu-routing.module';
import { BurgersListComponent } from './burgers-list/burgers-list.component';
import { BurgerCardComponent } from './burger-card/burger-card.component';
import { OrderDeliveryInfoComponent } from './order-delivery-info/order-delivery-info.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [MainInfoComponent, BurgersListComponent, BurgerCardComponent, OrderDeliveryInfoComponent, FooterComponent],
  imports: [
    CommonModule,
    MenuRoutingModule
  ]
})
export class MenuModule { }
