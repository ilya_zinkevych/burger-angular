import { Component, OnInit } from '@angular/core';
import { Burger } from 'src/shared/models/Burger';
import { BurgerService } from 'src/app/services/burger/burger.service';

@Component({
  selector: 'app-burgers-list',
  templateUrl: './burgers-list.component.html',
  styleUrls: ['./burgers-list.component.scss']
})

export class BurgersListComponent implements OnInit {
  burgers: Burger[];

  constructor(private burgerService: BurgerService) { }

  ngOnInit() {
    this.burgerService.getBurgersList()
      .subscribe(
        (res) => {
          this.burgers = res && res.filter(bur => bur.name);
        },
        (err) => {
          console.error(err);          
        }
      )
  }

}
