import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private API_URL = environment.apiUrl;

  constructor(private http: HttpClient) { }

  public getRequest<T>(path: string): Observable<T>{
    return this.http.get<T>(`${this.API_URL}/${path}`);
  }
}
