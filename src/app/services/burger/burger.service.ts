import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs';
import { Burger } from 'src/shared/models/Burger';

@Injectable({
  providedIn: 'root'
})
export class BurgerService {
  private address = 'burgers';
  constructor(private apiService: ApiService) { }

  public getBurgersList(): Observable<Burger[]> {
    console.log('dfq');
    
    return this.apiService.getRequest(this.address);
  }
}
